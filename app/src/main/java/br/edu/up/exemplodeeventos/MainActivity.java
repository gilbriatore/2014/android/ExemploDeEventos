package br.edu.up.exemplodeeventos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnJava = (Button) findViewById(R.id.btnJava);
        btnJava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                EditText txtNome = (EditText) findViewById(R.id.txtNome);
                EditText txtSobrenome  = (EditText) findViewById(R.id.txtSobrenome);

                String nome = txtNome.getText().toString();
                String sobrenome = txtSobrenome.getText().toString();

                Pessoa pessoa = new Pessoa(nome, sobrenome);

                Intent in = new Intent(MainActivity.this, SegundaActivity.class);


                Bundle pacote = in.getExtras();
                pacote.putSerializable("minhaPessoa", pessoa);
                //in.putExtra("minhaPessoa", pessoa);

                startActivity(in);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void metodoCallbakXML(View view) {

        String msg = "Teste do método de callback XML";
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();

    }
}
